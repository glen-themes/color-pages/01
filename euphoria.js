// hello welcome to my crib
// there's probably an easier way to do this but alas i used up all my brain cels

$(document).ready(function(){
    $(".color[color]").each(function(){
        var getColor = $(this).attr("color");
        $(this).css({"background":getColor});
        $(this).siblings(".hexcode").html(getColor);
    });
    
    var fadeSpeed = getComputedStyle(document.documentElement)
                   .getPropertyValue("--Filter-Fade-Speed"),
        rawSpeed = parseInt(fadeSpeed);
    
    $(".filter-color").click(function(){
        $(this).siblings().removeClass("chosen");
        if($(this).hasClass("chosen")) { 
            // see all
            $(this).removeClass("chosen");
            $(".inc").addClass("ghost");
            
            setTimeout(function(){
                $(".inc").removeClass("ghost");
                $(".one-color").fadeIn(rawSpeed);
            },rawSpeed);
            
        } else {
            // see one
            $(this).addClass("chosen");
            $(".one-color").stop(true,true).fadeOut(rawSpeed).filter("[type="+ $(this).attr("type") + "]").delay(rawSpeed).fadeIn(rawSpeed);
        }
    });//end click
});//end ready
